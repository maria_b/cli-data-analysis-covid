# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 11:07:59 2021

@author: Maria
"""

#Maria Barba 
#Import dataframes from stored SQL tables. 
#Compares values between dataframes for a chosen country by the user for yesterday, today and two days ago
#The values compared will be : deaths toll,number of cases (difference, %increase, %decrease)
#Compare these values with four other countries
#Plot Deaths/1M for 5 different countries
#Compare two datasets for 5 other countries (TOTLARECOVERED ,  TOTALTESTS) using a bar plot

from random import seed
from random import randint
import mysql.connector
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
class CompareValues:
    
    #write a constructor that will connect to the datatbase and 
    #will create a cursor used to retrieve all data from the database
    def __init__(self):
        #todayData will store a dataframe containing values from the "today" table in the database
        self.__todayData=""
        #yesterdayData will store a dataframe containing values from the "yesterday" table in the database
        self.__yesterdayData=""
        #twodaysagoData will store a dataframe containing values from the "twodaysago" table in the database
        self.__twodaysagoData=""
        #threedaysData will store key data for the selected country by the user (today,yesterday and two days ago)
        self.__threedaysData=""
        #create a list of random indexes used to determine countries which will by analyzed for their key numbers (4 indexes= 4 countries)
        self.__random_indx=[]
        #set columns for "period" (when %decrease)
        self.__period1=["Non Applicable"," Today/Yesterday","Yesterday/Two days ago"]
        #set column for "period" (when %increase & difference)
        self.__period0=[" Today/Yesterday","Yesterday/Two days ago","Non Applicable"]
        #create a connection to musql database
        try:
            self.__mydb = mysql.connector.connect(
              host="localhost",
              user="mar",
              password="luv123Mar!",
              database="testdb",
              auth_plugin='mysql_native_password'
            ) 
        #create a cusrsor from mysql database
            self.__mycursor = self.__mydb.cursor(buffered = True)
        #catch any connection errors
        except mysql.connector.Error as e:
            print("Error code:", e.errno )# error number
            print("SQLSTATE value:", e.sqlstate) # SQLSTATE value
            print("Error message:", e.msg)# error message
            print("Error:", e)# errno, sqlstate, msg values
            s = str(e)
            print("Error:", s) # errno, sqlstate, msg values
            

    #write a function that will import all data from the database 
    #using tables : today,yesterday and two days ago  and create separate dataframes  
    def importAllData(self):
        #fetch table for today
        self.__mycursor.execute('SELECT * FROM today')
        today_rows = self.__mycursor.fetchall()
        self.__todayData = pd.DataFrame(today_rows, columns=self.__mycursor.column_names)
        #fetch table for yesterday
        self.__mycursor.execute('SELECT * FROM yesterday')
        yesterday_rows = self.__mycursor.fetchall()
        self.__yesterdayData = pd.DataFrame(yesterday_rows, columns=self.__mycursor.column_names)
        #fetch table for 2 days ago 
        self.__mycursor.execute('SELECT * FROM twodaysago')
        twodaysago = self.__mycursor.fetchall()
        self.__twodaysagoData = pd.DataFrame(twodaysago, columns=self.__mycursor.column_names)
        #After all data is imported from the database and set ; close the cursor and the connection
        self.__mycursor.close()
        self.__mydb.close()
        
        
        #write a function that will import today, yesterday and two day's ago 
        #TotalCases and TotalDeaths and will build a dataframe for this country 
        #using the appropriate values. 
    def getDataSelectCountry(self,country):
        #find TotalCases and TotalDeaths for provided country today
        today_country=self.__todayData[["TotalCases","TotalDeaths"]].loc[self.__todayData["Country,Other"]==country]
        #find TotalCases and TotalDeaths for provided country yesterday
        yesterday_country=self.__yesterdayData[["TotalCases","TotalDeaths"]].loc[self.__yesterdayData["Country,Other"]==country]
        #find TotalCases and TotalDeaths for provided country twpdaysago
        twodaysago_country=self.__twodaysagoData[["TotalCases","TotalDeaths"]].loc[self.__twodaysagoData["Country,Other"]==country]
        #crate a dataframe for this country countaining today,yestreday and two days ago data
        self.__threedaysData=today_country.append( yesterday_country).append(twodaysago_country)
        
       
        
        #write a function that will analyze key numbers for the requested country by the user 
        #it will display %increase, %decrease and difference for TotalCases and TotalDeaths
    def analyzeKeyNumbers(self,country):
        #make sure TotalCases & TotalDeaths are numeric types so that they can be analyzed
        self.__threedaysData["TotalCases"] = pd.to_numeric(self.__threedaysData.TotalCases.str.replace(",", ""), errors='coerce')
        self.__threedaysData["TotalDeaths"] = pd.to_numeric(self.__threedaysData.TotalDeaths.str.replace(",", ""), errors='coerce')
        self.__threedaysData = self.__threedaysData.fillna(0)
        print("\n |---The difference of values for TotalCases,TotalDeaths is ---| :", country)
        # build a dataframe from a difference of TotalCases,TotalDeaths
        difference=pd.DataFrame(self.__threedaysData.diff(periods=-1))
        # Add a column to the appropriate period (difference found from down-->up on Dataframe)
        difference["Period"]=self.__period0
        print(difference)
        
        print("\n |---The %increase of values for TotalCases,TotalDeaths is :---|", country)
        #build a column for %increase of TotalCases,TotalDeaths
        total_increase=pd.DataFrame((self.__threedaysData.pct_change(periods=-1))*100)
        #Add a column to the appropriate period (increase found from down-->up on Dataframe)
        total_increase["Period"]=self.__period0
        print(total_increase)
        
        print("\n |---The %decrease of values for TotalCases,TotalDeaths is :---|", country)
        #build a column for %decrease of TotalCases,TotalDeaths
        total_decrease=pd.DataFrame((self.__threedaysData.pct_change(periods=1))*100)
        #Add a column to the appropriate period (decrease found from up-->down on Dataframe)
        total_decrease["Period"]=self.__period1
        print(total_decrease)
        
        #write a function that will Create 1 bar plot comparing the difference between the current death toll/ 1 m population 
        #for today, for 5 countries 
    def plotKeyDataCountries(self):
        #find Deaths/1M pop for today in Spain,UK,Argentina,Russia,Germany
        today_countries=self.__todayData[["Deaths/1M pop","Country,Other"]].loc[(self.__todayData["Country,Other"]=="Spain") | (self.__todayData["Country,Other"]=="UK") | (self.__todayData["Country,Other"]=="Argentina") | (self.__todayData["Country,Other"]=="Russia") | (self.__todayData["Country,Other"]=="Germany")]
        #transform Deaths/1M pop to numeric
        today_countries["Deaths/1M pop"]=pd.to_numeric(today_countries["Deaths/1M pop"].str.replace(",", ""))
        today_countries.plot.bar(x="Country,Other",y="Deaths/1M pop")
        #label appropriately the plot
        plt.title("TODAYDEATHSPERMILLION")
        plt.xlabel("Countries")
        plt.ylabel("Deaths per Million")
        
        #write a function to compare two sets of data for five  countries 
        #the compared data is : TOTALARECOVERED and TOTALTESTS
    def plotTwoDataSets(self):
        #find today's TOTALARECOVERED  and TOTALTESTS data for 5 countries : Brazil, India, Italy, Mexico, Canada
        today_recovered_tests=self.__todayData[["TotalRecovered","TotalTests", "Country,Other"]].loc[(self.__todayData["Country,Other"]=="Brazil") | (self.__todayData["Country,Other"]=="India") | (self.__todayData["Country,Other"]=='Italy') | (self.__todayData["Country,Other"]=='Mexico') | (self.__todayData["Country,Other"]=='Canada')]
        #make sure that TotalRecovered & TotalTests are numeric types so that they can be plotted
        today_recovered_tests["TotalRecovered"]=pd.to_numeric(today_recovered_tests["TotalRecovered"].str.replace(",", ""))
        today_recovered_tests["TotalTests"]=pd.to_numeric(today_recovered_tests["TotalTests"].str.replace(",", ""))
        #transform TotalRecovered Series to a list
        recovered=today_recovered_tests["TotalRecovered"].to_list()
        #transform TotalTests to a list
        tests=today_recovered_tests["TotalTests"].to_list()
        #transform Country,Other to a list
        index=today_recovered_tests["Country,Other"].to_list()
        #build a dataframe used for the subplot
        to_subplot = pd.DataFrame({'Total Tests': tests,
                    'Total Recovered': recovered}, index=index)
        #plot the subplots
        plot_axes = to_subplot.plot.bar(rot=0, subplots=True,figsize=(10,10))
        # set a title to the plot
        
        plt.suptitle("TOTAL RECOVERED AND TOTAL TESTS FOR 5 COUNTRIES")
      

        #write a function that will set a random index list used to determine in analyzeMultipleKeyNumbers function 4 countries 
        #used to compare TotalCases,TotalDeaths which will be different from the country provided by the user 
    def generateRandom(self):
        #create a seed object
        seed(1)
        #generate 4 random indexes and append them to index list
        for _ in range(4):
            val = randint(0, 100)
            self.__random_indx.append(val)
    
        
        #write a function that will print a dataframe for today,yesterday and two days ago difference,%increase and %decrease forr 4 other countries
        #different from the country provided by the user
    def analyzeMultipleKeyNumbers(self,country):
        #exclude the country provided by the user from the dataframe
        tdno_country= self.__todayData[["TotalCases","TotalDeaths","Country,Other"]].loc[self.__todayData["Country,Other"]!=country]
        #reset the dataframe index to account for the missing country
        tdno_country.reset_index(drop=True,inplace=True)
        #create a name_list with having the index from the random_indx list
        name_list=tdno_country[["Country,Other"]].iloc[self.__random_indx].squeeze().to_list()
        #find TotalCases & TotalDeaths for today, yesterday and two days ago
        dftoday=self.__todayData[["TotalCases","TotalDeaths","Country,Other"]].loc[(self.__todayData["Country,Other"] == name_list[0]) | (self.__todayData["Country,Other"] == name_list[1]) | (self.__todayData["Country,Other"] == name_list[2]) | (self.__todayData["Country,Other"] == name_list[3])]
        dfyesterday=self.__yesterdayData[["TotalCases","TotalDeaths","Country,Other"]].loc[ (self.__yesterdayData["Country,Other"] == name_list[0]) | (self.__yesterdayData["Country,Other"] == name_list[1]) | (self.__yesterdayData["Country,Other"] == name_list[2]) | (self.__yesterdayData["Country,Other"] == name_list[3])]
        dftwodaysago=self.__twodaysagoData[["TotalCases","TotalDeaths","Country,Other"]].loc[(self.__twodaysagoData["Country,Other"] == name_list[0]) | (self.__twodaysagoData["Country,Other"] == name_list[1]) | (self.__twodaysagoData["Country,Other"] == name_list[2]) | (self.__twodaysagoData["Country,Other"] == name_list[3])]
        #create a dataframe containing data for 4 countries for today,yesterday and two days ago
        threedays=dftoday.append(dfyesterday).append(dftwodaysago)
        #transform TotalCases & TotalDeaths to numeric types
        threedays["TotalCases"] = pd.to_numeric(threedays.TotalCases.str.replace(",", ""))
        threedays["TotalDeaths"] = pd.to_numeric(threedays.TotalDeaths.str.replace(",", ""))
        
        #Print difference of data for each country 
        for random_country in name_list:
             #build a dataframe from a difference,%increase & decrease of TotalCases,TotalDeaths
             country_difference=pd.DataFrame(threedays[["TotalCases","TotalDeaths"]].loc[(threedays["Country,Other"] == random_country)].diff(periods=-1))
             total_increase=pd.DataFrame((threedays[["TotalCases","TotalDeaths"]].loc[(threedays["Country,Other"] == random_country)].pct_change(periods=-1))*100)
             total_decrease=pd.DataFrame((threedays[["TotalCases","TotalDeaths"]].loc[(threedays["Country,Other"] == random_country)].pct_change(periods=1))*100)
             #Add a column to the appropriate period 
             country_difference["Period"]=self.__period0
             total_increase["Period"]=self.__period0
             total_decrease["Period"]=self.__period1
             #Print difference of data for each country
             print("\n |---The difference of values for TotalCases,TotalDeaths is ---| :", random_country)
             print(country_difference)
             #Print %increase of data for each country
             print("\n |---The %increase of values for TotalCases,TotalDeaths is ---| :", random_country)
             print(total_increase)
             #Print %decrease of data for each country
             print("\n |---The %decrease of values for TotalCases,TotalDeaths is :---|", random_country)
             print(total_decrease)


# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 12:07:49 2021

@author: Maria
"""
#Maria Barba 
#Import all the necessary modules to make the main program run
from retrieve_data import Covid_Data
from archive_database import ArchiveMySQL
from compareValues import CompareValues

#First create a Covid_Data object to set website.html file and to retrieve all data from the website 
# --> https://www.worldometers.info/coronavirus
covid_data=Covid_Data()
#write the website source code to the local website.html file
covid_data.writeSourceCode("website.html")
#create a soup object using the local website.html file
soup=covid_data.createSoupFromHTML("website.html")
#scrape covid data table for today
covid_data.scrapeTable("main_table_countries_today",soup)
#scrape covid data table for yesterday
covid_data.scrapeTable("main_table_countries_yesterday",soup)
#scrape covid data table for 2 days ago
covid_data.scrapeTable("main_table_countries_yesterday2",soup)
#add all scraped tables together
covid_data.dictList()
#write all scraped tables to local JSON file named "country_neighbour_dist_file.json"
covid_data.writeToJSON()


#Second,create an ArchiveMySQL object to save all tables from local JSON file to the database
sql_tables = ArchiveMySQL()
sql_tables.importToMySQL()

#Create a CompareValues object and import all tables from database

compared_tables=CompareValues()
compared_tables.importAllData()
    
#Show the user a list of countries and promt to choose one country
print("\n ---A list of countries to choose your country from --- \n")
i=0
for country in compared_tables._CompareValues__todayData["Country,Other"].to_list():
        to_print="{}--> {}"
        i+=1
        print(to_print.format(i, country))
    
#Ask the user to input a country from a list of countries 
try:
    country = str(input("\n |-Please enter a country name from the following list with the first letter capital:-|\n"))
    chosen='\n |----Your chosen country is ---->>>> {} ----|'
    print(chosen.format(country))
    #If the provided country by the user does not exist , exit the program
    if country not in compared_tables._CompareValues__todayData["Country,Other"].to_list():
        print("\n |----Not a real country !----|")
        exit()
    # Analyse key numbers for chosen country and 4 other countries
    compared_tables.getDataSelectCountry(country)
    compared_tables.analyzeKeyNumbers(country)
    compared_tables.generateRandom()
    compared_tables.analyzeMultipleKeyNumbers(country)
 
    #Display plotted data for Deaths 1/m
    compared_tables.plotKeyDataCountries()  
    #Display plot icluding two data sets for 5 countries : Total Recovered & Total Tests
    compared_tables.plotTwoDataSets()
    
    print("\n To check the plots click on the plots button near variable explorer .  ")
except ValueError:
    print("\n |----Please enter a country name, not a number.----|")
except:
    print("\n |---------Wrong country input, try again.---------| \n |---- Write the country like seen on the list.----|")
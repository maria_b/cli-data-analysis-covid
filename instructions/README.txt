------------------------------------------------------------------PROJECT 1 INSTRUCTIONS--------------------------------------------------------------------------
MARIA BARBA 24/03/2021

1.Do not remove website.html from local project folder

2.Do not remove country_neighbour_dist_file.json from local project folder

3.main.py makes the main program run

4.pymysql module is necessary for archive_database.py file

5.In the archive_database.py file, change the engine link following this format  : mysql+pymysql://username:password@localhost/database .
|-->self._engine= create_engine('mysql+pymysql://mar:luv123Mar!@localhost/testdb') replacing mar by mysql workbench user, 
    replacing luv123Mar! by mysql password and testdb should be replaced by mysql database name.

6.In the compareValues.py file , in the __init__ part of the class the connection credentials need to be changed to the correct username,password,
and database.

File descriptions:
------------------

retrieve_data.py

This module  contains the necessary functions to retrieve the html source code from the covid 
webpage into a local folder html file called website.html. Then it will scrape through the html source code
to create a dictionnary object for the appropriate table (today,yesterday and 2 days ago). Each table will
be appendend to a dictionnary list which will be used to set the country_neighbour_dist_file.json.

archive_database.py

This module takes the local JSON file data from country_neighbour_dist_file.json and will
use each covid table data to implement three tables for today, yesterday and two days ago in MySQL. 

compareValues.py

Imports dataframes from stored SQL tables. 
Compares values between dataframes for a chosen country by the user for yesterday, today and two days ago
The values compared will be : deaths toll,number of cases (difference, %increase, %decrease)
Compare these values with four other countries
Plots Deaths/1M for 5 different countries
Compare two datasets for 5 other countries (TOTAL RECOVERED ,  TOTAL TESTS) using a bar plot.

main.py

Imports values from retrieve_data.py,archive_database.py,compareValues.py to make main program run . The user enters a country of his choice from
the country list and an analysis of deaths toll,number of cases is printed for this country (difference,%increase & %decrease). Then the same analysis is printed 
for 4 other countries in the same format. Two plots are created . The first plot compares Deaths/1M for 5 different countries and the 
second plot compares two datasets for five other countries (TOTAL RECOVERED, TOTAL TESTS ). If the user provides a wrong country (not a real country), 
the program stops running.If the input is not a string, the program also stops running.

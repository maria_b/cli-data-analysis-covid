# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 14:41:32 2021

@author: Maria
"""
#Maria Barba 

#This module takes the local JSON file data from country_neighbour_dist_file.json and will
#use each covid table data to implement three tables for today, yesterday and 
#two days ago inside MySQL database. 
import json
from sqlalchemy import create_engine
import pandas as pd 
import pymysql
import pymysql.cursors
class ArchiveMySQL:
    
    #create a constructor to connect to my sql database and also create a cursor from my database connection
      def __init__(self):
          #follow this format to create an engine mysql+pymysql://username:password@localhost/database (need to install the pymysql module first)
          #like explained in this documentation : https://docs.sqlalchemy.org/en/14/core/engines.html
          self._engine= create_engine('mysql+pymysql://mar:luv123Mar!@localhost/testdb')

      #create a function that will retrieve a disctionnary list of the chosen table from the JSON file and 
      #will create the appropriate tables in mysql 
      #table names can be either : today,yesterday or twodaysago
      def importToMySQL(self):
        
       try:  
              with open("country_neighbour_dist_file.json", "r") as read_file:
                  #read data from local json file
                  data = json.load(read_file)
                  #create dataframe from local json file
                  df=pd.DataFrame(data)
                  #Adjust columns
                  df.columns = df.columns.str.strip()    
                  #Create today's covid data dataframe (from index 1 to 222)
                  today_df=df.iloc[1:222]
                  #Create yesterday's covid data dataframe (from index 223 to 444)
                  yesterday_df=df.iloc[223:444]
                  #Create two days ago dataframe (from index 445 to 666)
                  twodays_df=df.iloc[445:666]
                  
                  #Load to mysql today's dataframe to today's table (omitting index and replacing table values if table exists)
                  today_df.to_sql(name="today",con= self._engine,index=False,if_exists='replace')
                  #Load to mysql yesterday's dataframe to yesterday's table (omitting index and replacing table values if table exists)
                  yesterday_df.to_sql(name="yesterday",con= self._engine,index=False,if_exists='replace')
                  #Load to mysql two days ago dataframe to two days ago table (omitting index and replacing table values if table exists)
                  twodays_df.to_sql(name="twodaysago",con= self._engine,index=False,if_exists='replace')
                  #dispose of connection when finished importing all tables to mysql
                  self._engine.dispose()
       except IOError:
            print ("Could not read file:", "country_neighbour_dist_file.json")

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 19:26:40 2021

@author: Maria
"""
#Maria Barba 

# This module  contains the necessary functions to retrieve the html source code from the covid 
#webpage into a local folder html file called website.html. Then it will scrape through the html source code
#to create a dictionnary object for the appropriate table (today,yesterday and 2 days ago). Each table will
#be appendend to a dictionnary list which will be used to set the country_neighbour_dist_file.json.

from bs4 import BeautifulSoup
import requests
import json
class Covid_Data :
    
    #create a constructor to set the url and untialize the https request
    def __init__(self):
        self.__url = "https://www.worldometers.info/coronavirus/"
        self.__request = requests.get("https://www.worldometers.info/coronavirus/")
        #create a Beautiful Soup Object which will directly use the webpage to set the html file
        self.__soup_html  = BeautifulSoup(self.__request.text, "html.parser")
        #create a list to store today's table
        self.__today=[]
        #create a list to store yesterday's table
        self.__yesterday=[]
        #create a list to store two days ago table
        self.__twodaysago=[]
        #create a list to store multiple tables
        self.__tablelist=[]


    #write a function to retrieve html source code 
    def writeSourceCode(self,filename_html):
        try:
            #open local "website.html" file and put the source code of the webpage
           f=open(filename_html,"w",encoding="utf-8")
           f.write(str(self.__soup_html))
           f.close()
        except IOError:
                print ("Could not read file:", filename_html)
     #write a function to create and return a beautiful soup object from the local html file
    def createSoupFromHTML(self,filename_html):
        try:
        #open local website.html file and read its contents to create a new soup object
           with open(filename_html, encoding='utf8') as html:
               soup_obj = BeautifulSoup(html, "html.parser")
           html.close()
        except IOError:
            print ("Could not read file:", filename_html)
        return soup_obj
    #write a function to create a dictionnary list of a specific table scraped from the local html file
    #tablename corresponds to table id in the source html : main_table_countries_today, main_table_countries_yesterday or main_table_countries_yesterday2
    def scrapeTable(self,tablename,soup):
        #find table with table we need to retrieve (today,yesterday or 2 days ago)
        today_table=soup.find("table", id=tablename)
        #find the table headings and store them in a list replacing "Tests/\n1M pop\n" by "Tests 1M pop\n" to avoid file read errors later
        table_headers=[ heading.text.replace("Tests/\n1M pop\n","Tests 1M pop\n") for heading in today_table.find_all("th") ]
        #find all table rows without a class containing the actual coutries and not continents data and store them in a list
        table_rows=[ row for row in today_table.find_all("tr",class_=None)]
        #build a dictonnary list where the table header is the key and the row is the value 
        day_results=[{ table_headers[index]:cell.text for index,cell in enumerate(row.find_all("td")) } for row in table_rows]
        #set table data according to the provided table name
        if tablename=="main_table_countries_today":
            self.__today=day_results
            
        elif  tablename=="main_table_countries_yesterday":
            self.__yesterday=day_results
        #if the table name is "main_table_countries_yesterday2"
        else :
           self.__twodaysago=day_results
    
    #write a function that will write all tables into a json file named country_neighbour_dist_file.json
    def writeToJSON(self):
        try:
            #open a local JSON file and write all of the table rows (from 3 tables) to the file
            with open("country_neighbour_dist_file.json", "w") as json_file:
                    json.dump(self.__tablelist, json_file, indent=4)
                    json_file.close()
        except IOError:
            print ("Could not write file:", "country_neighbour_dist_file.json")
    #write a function that will create a dictionnary list 
    def dictList(self):
            #add all of the dictionnary lists together for each table to create a big
            #dictionnary list that will be written to the local JSON file (respecting the JSON format)
            self.__tablelist=self.__today + self.__yesterday + self.__twodaysago
            
    


      

   
   
    